<?php include ROOT.'/views/layouts/header.php';?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-xs-12">
            
            <img src="/images/sun1.png" class="img-fluide" id="sun">
            <img src="/images/cloud.png" class="img-responsive" id="cloud">
            
            <div class="weather">
                <h4>Погода в Запорожье на <?php echo date("d.m.Y");?>г.</h4>
                <table class="table table-responsive">
                    <tr><td>Облачность:</td><td><?=$weather['cloudness']?></td></tr>
                    <tr><td>Температура:</td><td><?=$weather['temp']?></td></tr>
                    <tr><td>Ветер:</td><td><?=$weather['wind']?></td></tr>
                    <tr><td>Давление:</td><td><?=$weather['barp']?></td></tr>
                    <tr><td>Влажность:</td><td><?=$weather['hum']?></td></tr>
                    <tr><td>Температура воды:</td><td><?=$weather['water']?></td></tr>
                </table>
        </div>
    </div>
</div>
 </div>   
<?php include ROOT.'/views/layouts/footer.php';?>
