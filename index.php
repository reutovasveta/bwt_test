<?php
use app\components\Router;

//отображение ошибок на время тестирования сайта
ini_set('display_errors',1);
error_reporting(E_All);

session_start();

define('ROOT',dirname(__FILE__));
define('DS',DIRECTORY_SEPARATOR);
require_once(ROOT.'/components/Autoload.php');
$autoloader = new Autoloader();
$autoloader->register();

$router = new Router;
$router->run();