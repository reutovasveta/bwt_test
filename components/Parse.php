<?php
namespace app\components;

class Parse{
    
    public static function getData($file,$start,$end){
        $pos = strpos($file, $start);
        if($pos===false) return 0;
        $str1 = substr($file, $pos);
        return strip_tags(substr($str1,0,strpos($str1, $end)));
    }
}

