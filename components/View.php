<?php
namespace app\components;

class View{
    
    public static function render($path,$data=null){
       if(is_array($data)){
           extract($data);
       }
        require (ROOT.'/views/'.$path.'.php');
       
    }
}