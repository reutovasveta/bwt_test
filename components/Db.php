<?php 

namespace app\components;

class Db{
    private static $db;
    
    private function __construct() {
        try {
            self::$db = new \PDO("mysql:host=localhost;dbname=bwt_test",'root','');
            self::$db->exec("set names utf8");
        }
        catch (PDOException $e) {
            echo "Connection Error: " . $e->getMessage();
        }

    }
    
    public static function getConnection() {
        if (!self::$db) {
           new Db();
        }
        return self::$db;
        }
        
}