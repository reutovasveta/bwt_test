<?php

class Autoloader {
     public function register(){
        spl_autoload_register(array($this, 'autoload'));
    }

    protected function autoload($class){
        $pathParts = explode('\\', $class);
        if (is_array($pathParts)){
            $prefix = array_shift($pathParts);
            $filePath = ROOT.DS.implode('/', $pathParts).'.php';
            require_once $filePath;
            return true;
        }
        return false;
    }
	
}


