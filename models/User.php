<?php
namespace app\models;
use app\components\Db;

class User{
    
    public static function checkName($name){
        $pattern='/^[a-zа-яё]{2,30}$/ui';
         if(preg_match($pattern, $name)){
             return true;
        }
        return false;
    }
    
    public static function checkEmail($email){
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            return true;
        }
        return false;
    }
    
    public static function checkBirthday($birthday){
        $pattern='/^[0-9]{4}-[0-1][0-9]-[0-3][0-9]$/';
        $split = explode('-', $birthday);
        if(preg_match($pattern, $birthday)){
            if (checkdate($split[1], $split[2], $split[0])){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    
    public static function checkMessage($message){
        if (mb_strlen($message) > 2 && mb_strlen($message) < 2000){
            return true;
        }
        return false;
    }
    public static function checkPassword($password){
        $pattern='/^[^\'<>&"]{6,20}$/u';
         if(preg_match($pattern, $password)){
             return true;
        }
        return false;
    }
    public static function signup($array){
        $db=Db::getConnection();
        $sql='INSERT INTO users (name,surname,email,password,gender,birthday) VALUES (:name,:surname,:email,:password,:gender,:birthday)';
        $result=$db->prepare($sql);
        $result->bindParam(':name',$array['name'],\PDO::PARAM_STR);
        $result->bindParam(':surname',$array['surname'],\PDO::PARAM_STR);
        $result->bindParam(':email',$array['email'],\PDO::PARAM_STR);
        $result->bindParam(':password',md5($array['password']),\PDO::PARAM_STR);
        $result->bindParam(':gender',$array['gender'],\PDO::PARAM_STR);
        $result->bindParam(':birthday',$array['birthday'],\PDO::PARAM_STR);
        return $result->execute();
    }
    
    public static function checkUserData($array){
        $db=Db::getConnection();
        $sql='SELECT * FROM users WHERE email=:email AND name=:name AND password=:password';
        $result=$db->prepare($sql);
        $result->bindParam(':name',$array['name'],\PDO::PARAM_STR);
        $result->bindParam(':email', $array['email'], \PDO::PARAM_STR);
        $result->bindParam(':password', md5($array['password']), \PDO::PARAM_STR);
        $result->setFetchMode(\PDO::FETCH_ASSOC);
        $result->execute();
        $row=$result->fetch();
        return $row;
    }
    
     public static function auth($id){
        $_SESSION['user']=$id;
    }
    
    public static function isGest(){
        if(isset($_SESSION['user'])){
            return false;
        }
         return true;
    }
    
}

