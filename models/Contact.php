<?php

namespace app\models;
use app\components\Db;

class Contact{
    
    public static function saveMessage($data){
        $db=Db::getConnection();
        $sql='INSERT INTO contacts (name_user,email_user,message) VALUES (:name_user,:email_user,:message)';
        $result=$db->prepare($sql);
        $result->bindParam(':name_user', $data['name_user'], \PDO::PARAM_STR);
        $result->bindParam(':email_user', $data['email_user'], \PDO::PARAM_STR);
        $result->bindParam(':message', $data['message'], \PDO::PARAM_STR);
        return $result->execute();
    }
    
   
     public static function getMessage(){
         $db=Db::getConnection();
         $result=$db->query('SELECT * FROM contacts');
         $result->setFetchMode(\PDO::FETCH_ASSOC);
         $messages=array();
         $i=0;
         while($row=$result->fetch()){
             $messages[$i]['date']=$row['date'];
             $messages[$i]['name']=$row['name'];
             $messages[$i]['message']=$row['message'];
             $i++;
         }
         return $messages;
     }
}

