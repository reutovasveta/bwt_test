<?php 

namespace app\controllers;

use app\models\User;
use app\models\Contact;
use app\components\Parse;
use app\components\View;

class SiteController{

    public function actionIndex(){
        View::render('site/index');
        return true;
    }
    
    public function actionSignup(){
        $data=array();
        if(isset($_POST['submit'])){
            $data['name'] = trim($_POST['name']);
            $data['surname'] = trim($_POST['surname']);
            $data['email'] = trim($_POST['email']);
            $data['password'] = trim($_POST['password']);
            $data['gender'] = $_POST['gender'];
            $data['birthday'] = $_POST['date'];
 
            $errors=false;

            if(!User::checkName($data['name'])){
              $errors[]='Введите имя не менее 2-х и не более 30 символов, используя русский или латинский алфавит!';			
            }
            if(!User::checkName($data['surname'])){
                $errors[]='Введите фамилию не менее 2-х и не более 30 символов, используя русский или латинский алфавит!';			
            }
            if(!User::checkEmail($data['email'])){
                 $errors[]='Введите корректный email!';			
              }
            if($birthday){
                 if(!User::checkBirthday($data['birthday'])){
                    $errors[]='Введите корректную дату!';			
                }
            }
            if(!User::checkPassword($data['password'])){
                $errors[]='Введите пароль от 6 до 20 символов! Нельзя использовать символы: & " \' < >';			
            }
                      
            if(!$errors){
                $result=User::signup($data);
            }
            
        }
        View::render('site/signup',['result'=>$result,'errors'=>$errors,'data'=>$data]);
        
        return true;
    }
    
    public function actionLogin(){
        $data=[];
        if(isset($_POST['submit'])){
            $data['name']=$_POST['name'];
            $data['email']=$_POST['email']; 
            $data['password']=$_POST['password'];
            
            $user=User::checkUserData($data);
                if(!$user){
                    $errors[]='Не верные данные для входа на сайт!';
               }else{
                  User::auth($user['id']);
                }
        }
        View::render('site/login',['user'=>$user,'errors'=>$errors,'data'=>$data]);
        return true;
    }
    public function actionLogout(){
        unset($_SESSION['user']);
        header('Location:/');
        return true;
    }
    
    public function actionWeather(){
        if(!User::isGest()){
            $file = file_get_contents('https://www.gismeteo.ua/weather-zaporizhia-5093/');
            
            $weather['cloudness'] = Parse::getData($file, '<dl class="cloudness">','</td>');
            $weather['temp'] = Parse::getData($file, '<div class="temp">','</dd>');
            $weather['wind'] = Parse::getData($file, '<div class="wicon wind">','</dd>');
            $weather['barp'] = Parse::getData($file, '<div class="wicon barp" title="Давление"','</dd>');
            $weather['hum'] = Parse::getData($file, '<div class="wicon hum" title="Влажность">','</div>');
            $weather['water'] = Parse::getData($file, '<div class="wicon water" title="Температура воды">','</dd>');
            
        }else{
            $error='Доступ только зарегистрированным пользователям';
        }
        View::render('site/weather',['weather'=>$weather]);
        return true;
    }
    
    public function actionFeedback(){
        $errors=false;
        $data=[];
        if(isset($_POST['captcha'])){
            $data['name_user']=trim($_POST['name_user']);
            $data['email_user']=trim($_POST['email_user']);
            $data['message']=trim(htmlspecialchars($_POST['message']));
        }
        
        if(isset($_POST['submit'])){
            $data['name_user']=trim($_POST['name_user']);
            $data['email_user']=trim($_POST['email_user']);
            $data['message']=trim(htmlspecialchars($_POST['message']));
            $captcha=$_SESSION['captcha'];
            $captcha_user=$_POST['captcha'];
            
            if($captcha != $captcha_user){
               $errors[]='Введите правильно код!'; 
            }
            if(!User::checkName($data['name_user'])){
                $errors[]='Имя может содержать только русские и английские буквы, длинной не менее 2-х и не более 30-ти символов!';
            }
            if(!User::checkEmail($data['email_user'])){
                 $errors[]='Введите корректный email!';			
            }
            if(!User::checkMessage($data['message'])){
                 $errors[]='Напишите сообщение длинной от 3-х до 2000 символов!';			
            }

            if(!$errors){
                  $result = Contact::saveMessage($data);
            }
        }
        View::render('site/feedback',['result'=>$result,'errors'=>$errors,'data'=>$data]);
        return true;
    }
    
     public function actionMessage(){
        if(!User::isGest()){
            $messages=Contact::getMessage(); 
        }else{
            $error='Доступ только зарегистрированным пользователям';
        }
        View::render('site/message',['error'=>$error,'messages'=>$messages]);
        return true;
     }
}
	
